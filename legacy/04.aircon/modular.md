---
title: AirCon Status
menu: AirCons
onpage_menu: true
expires: 300
body_classes: "modular fullwidth"

content:
    items: @self.modular
    order:
        by: default
        dir: asc
        custom:
            - _top
            - _aircon0
            - _aircon1
---
